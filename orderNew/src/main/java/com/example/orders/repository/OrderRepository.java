package com.example.orders.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.orders.model.OrderModel;

public interface OrderRepository extends JpaRepository<OrderModel, Integer> {

}
