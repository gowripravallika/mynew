package com.example.orders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class Eureka10Application {

	public static void main(String[] args) {
		SpringApplication.run(Eureka10Application.class, args);
	}

}
