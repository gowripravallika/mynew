package com.example.orders.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.orders.model.OrderModel;
import com.example.orders.repository.OrderRepository;


@RestController
public class OrderController {

	@Autowired
	OrderRepository orderRepository;
	
	@PostMapping("/add")
	public OrderModel addOrder(@RequestBody OrderModel order) {
		return orderRepository.save(order);

	}

	@GetMapping("/get")
	public List<OrderModel> getUser() {
		return orderRepository.findAll();
	}

	@PutMapping("/update")
	public OrderModel updateOrder(@RequestBody OrderModel order) {
		return orderRepository.save(order);
	}

//deleting a particular field
	@RequestMapping("delete/{id}")
	public void deleteOrder(@PathVariable int id) {
		orderRepository.deleteById(id);
	}
	
}
